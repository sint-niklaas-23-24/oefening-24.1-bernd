﻿using System;
using System.Windows;

namespace Dobbelsteen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           
           
        }
        Random randomWaarde = new Random();
        private void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            
            WorpDobbelsteen eersteRol = new WorpDobbelsteen(4, randomWaarde);
            eersteRol.Roll();
            lblRol1.Content = eersteRol.Waarde;
            WorpDobbelsteen tweedeRol = new WorpDobbelsteen(); //default constructor
            tweedeRol.Roll();
            lblRol2.Content = tweedeRol.Waarde;
        }
    }
}