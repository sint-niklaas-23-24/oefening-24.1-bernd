﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Dobbelsteen
{
    internal class WorpDobbelsteen
    {
        //Atributen
        private int _aantalZijden;
        private int _waarde;
        private Random _willGetal;
        //Constructor
        public WorpDobbelsteen()
        {
            AantalZijden = 6;
            WillGetal = new Random();
        }
        public WorpDobbelsteen (Random r) //default constructor rood
        {
            WillGetal = r;
            AantalZijden = 6;
        }
        public WorpDobbelsteen(int aantalZijden, Random r) //blauw
        {
            WillGetal = r;
            AantalZijden = aantalZijden;
        }
        //Properties
        public int AantalZijden
        {
            get { return _aantalZijden; }
            set { _aantalZijden = value; }
        }
        public int Waarde
        {
            get { return _waarde; }
            set { _waarde = value; }
        }
        public Random WillGetal
        {
            get { return _willGetal; }
            set { _willGetal = value; }
        }
        //Methoden
        public void Roll()
        {
            Waarde = WillGetal.Next(1, AantalZijden+1);  
        }
    }
}
