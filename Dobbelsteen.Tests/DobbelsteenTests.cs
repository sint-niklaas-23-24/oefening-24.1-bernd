﻿namespace Dobbelsteen.Tests
{

    class DobbelsteenTests
    {
        [Test]
        public void InterneTest()
        {
            //Arrange
            Random rnd = new Random();
            int rol1, rol2;
            //Act
            rol1 = rnd.Next();
            rol2 = rnd.Next();
            //Assert
            Assert.IsTrue(rol1 >= 0 && rol2 >= 0);
        }

    }
}
